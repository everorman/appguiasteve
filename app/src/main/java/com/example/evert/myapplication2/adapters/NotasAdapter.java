package com.example.evert.myapplication2.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ValueCallback;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.evert.myapplication2.R;
import com.example.evert.myapplication2.activities.BaseActivity;
import com.example.evert.myapplication2.activities.NotasActivity;
import com.example.evert.myapplication2.data.EsteveDataSource;
import com.example.evert.myapplication2.models.Nota;

import java.util.ArrayList;

/**
 * Created by Evert on 25-09-2015.
 */
public class NotasAdapter extends RecyclerView.Adapter<NotasAdapter.ViewHolder> {
    private EsteveDataSource dataSource;
    private ArrayList<Nota> notas;
    private int itemLayout;

    public  NotasAdapter(ArrayList<Nota> notas,int itemLayout){
        this.notas = notas;
        this.itemLayout = itemLayout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(itemLayout,viewGroup,false);
        dataSource = new EsteveDataSource(v.getContext());
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Nota nota = notas.get(i);
        //viewHolder.titulo.setText(nota.getNombre());
        viewHolder.nota.setText(nota.getNota());
        viewHolder.articulo.setText(nota.getArticulo());
        viewHolder.id_articulo.setText(String.valueOf(nota.getIdArticulo()));
        viewHolder.id_nota.setText(String.valueOf(nota.getId()));
        viewHolder.boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(v.getContext());
                builder1.setMessage("¿Desea eliminar la nota?");
                builder1.setCancelable(true);
                builder1.setPositiveButton("Si",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                builder1.setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return notas.size();
    }

    public static class ViewHolder extends  RecyclerView.ViewHolder{
        //public TextView titulo;
        public TextView nota;
        public TextView articulo;
        public ImageButton boton;
        public TextView id_articulo;
        public TextView id_nota;

        public ViewHolder(final View itemView) {
            super(itemView);

            //titulo = (TextView) itemView.findViewById(R.id.titulo);
            nota = (TextView) itemView.findViewById(R.id.contenido);
            articulo = (TextView) itemView.findViewById(R.id.articulo);
            boton = (ImageButton) itemView.findViewById(R.id.imageButton2);
            id_articulo = (TextView) itemView.findViewById(R.id.id_nota_articulo);
            id_nota = (TextView) itemView.findViewById(R.id.id_nota);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(view.getContext(), BaseActivity.class);

                    intent.putExtra("back","2");
                    intent.putExtra("articulo", id_articulo.getText());
                    intent.putExtra("titulo", articulo.getText());
                    intent.putExtra("contenido", nota.getText());

                    view.getContext().startActivity(intent);
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(v.getContext());
                        final LayoutInflater inflater = LayoutInflater.from(v.getContext());
                        final View root = inflater.inflate(R.layout.dialogo_layout, null);
                        alertDialogBuilder.setTitle("Nota:");
                        alertDialogBuilder.setView(root);
                        final TextView nota_contenido = (TextView) root.findViewById(R.id.nota_cuerpo);
                        nota_contenido.setText(nota.getText());

                        // set positive button: Yes message
                        alertDialogBuilder.setPositiveButton("Guardar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if(nota_contenido.getText()!=nota.getText()){
                                    EsteveDataSource dataSource = new EsteveDataSource(inflater.getContext());
                                    dataSource.upDateSeleccion(id_nota.getText().toString(),nota_contenido.getText().toString());
                                    NotasActivity activity = (NotasActivity) inflater.getContext();
                                    activity.finish();
                                    inflater.getContext().startActivity(activity.getIntent());
                                }

                            }
                        });
                        // set negative button: No message
                        alertDialogBuilder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // cancel the alert box and put a Toast to the user
                                dialog.cancel();
                            }
                        });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        // show alert
                        alertDialog.show();
                        return true;
                    }


            });
        }
    }


}
