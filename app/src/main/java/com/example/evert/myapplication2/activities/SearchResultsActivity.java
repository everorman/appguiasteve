package com.example.evert.myapplication2.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.widget.TextView;

import com.example.evert.myapplication2.R;
import com.example.evert.myapplication2.adapters.ArticuloAdapter;
import com.example.evert.myapplication2.data.EsteveDataSource;
import com.example.evert.myapplication2.models.Articulo;

import java.util.ArrayList;

public class SearchResultsActivity extends Activity {

    private TextView txtQuery;
    private EsteveDataSource dataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results);

        // get the action bar
        ActionBar actionBar = getActionBar();

        // Enabling Back navigation on Action Bar icon
        actionBar.setDisplayHomeAsUpEnabled(true);
        dataSource = new EsteveDataSource(this);
        txtQuery = (TextView) findViewById(R.id.txtQuery);
        ArrayList<Articulo> articuloArrayList = new ArrayList<>();
        articuloArrayList = dataSource.busqueda(txtQuery.getText().toString());


        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_resultados);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(new ArticuloAdapter(articuloArrayList, R.layout.layout_articulo_item));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        handleIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    /**
     * Handling intent data
     */
    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);

            /**
             * Use this query to display search results like
             * 1. Getting the data from SQLite and showing in listview
             * 2. Making webrequest and displaying the data
             * For now we just display the query only
             */
            txtQuery.setText("Search Query: " + query);

        }

    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            Intent intent = new Intent(this,BaseActivity.class);
            intent.putExtra("back","0");
            intent.putExtra("contenido","1");
            startActivity(intent);

        }
        return false;
    }
}
