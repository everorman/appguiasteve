package com.example.evert.myapplication2.fragments;


import android.graphics.Point;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.evert.myapplication2.R;


public class IndexFragment extends Fragment {
    private View root;
    public IndexFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_index, container, false);
        ImageView imageView = (ImageView) root.findViewById(R.id.imageView);
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y /3;

        imageView.getLayoutParams().height = height;
        imageView.getLayoutParams().width = height+100;
        Log.e("IndexFragment",""+height);
        return root;
    }
}
