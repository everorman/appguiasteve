package com.example.evert.myapplication2.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.provider.BaseColumns;
import android.util.Log;

import com.example.evert.myapplication2.models.Articulo;
import com.example.evert.myapplication2.models.Nota;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by Evert on 2/12/2015.
 */
public class EsteveDataSource {

    //Informacion de la BD

    public static final String INDICE_TABLE_NAME = "Indice";
    public static final String CONTENIDO_TABLE_NAME = "Contenido";
    public static final String SELECCION_TABLE_NAME = "Seleccion";
    public static final String STRING_TYPE = "text";
    public static final String INT_TYPE = "integer";

    //Campos de las tablas
    public static class ColumnIndice{
        public static final String ID_INDICE = BaseColumns._ID;
        public static final String IDENTIFICADOR_INDICE = "id";
        public static final String PADRE_INDICE = "padre";
        public static final String TITULO_INDICE = "titulo";
        public static final String CONTENIDO_CUERPO = "contenido";
    }

    public static class ColumnContenido{
        public static final String ID_CONTENIDO = BaseColumns._ID;
        public static final String IDENTIFICADOR_CONTENIDO = "id";
        public static final String CUERPO_CONTENIDO = "cuerpo";
    }
    public static  class ColumnSeleccion{
        public static final String ID_SELECCION = BaseColumns._ID;
        public static final String IDENTIFICADOR_SELECCION = "id";
        public static final String ARTICULO_SELECCION = "articulo";
        public static final String TIPO_SELECCION = "tipo";
        public static final String TARGET_SELECCION = "target";
        public static final String CUERPO_SELECCION = "cuerpo";

    }

    //Script para crear las tablas e insertar datos iniciales
    public static final String CREATE_INDICE_SCRIPT =
            "create table "+INDICE_TABLE_NAME+"("+
                ColumnIndice.ID_INDICE+" "+INT_TYPE+" primary key autoincrement,"+
                ColumnIndice.IDENTIFICADOR_INDICE+" "+INT_TYPE+" not null,"+
                ColumnIndice.PADRE_INDICE+" "+INT_TYPE+" not null,"+
                ColumnIndice.TITULO_INDICE+" "+STRING_TYPE+" not null,"+
                ColumnIndice.CONTENIDO_CUERPO+" "+STRING_TYPE+")";

    public static final String CREATE_CONTENIDO_SCRIPT =
            "create table "+CONTENIDO_TABLE_NAME+"("+
                ColumnContenido.ID_CONTENIDO+" "+INT_TYPE+" primary key autoincrement,"+
                ColumnContenido.IDENTIFICADOR_CONTENIDO+" "+INT_TYPE+" not null,"+
                ColumnContenido.CUERPO_CONTENIDO+" "+STRING_TYPE+" not null)" ;

    public static final String CREATE_SELECCION_SCRIPT =
            "create table "+SELECCION_TABLE_NAME+"("+
                    ColumnSeleccion.ID_SELECCION+" "+INT_TYPE+" primary key autoincrement,"+
                    ColumnSeleccion.IDENTIFICADOR_SELECCION+" "+INT_TYPE+" not null,"+
                    ColumnSeleccion.ARTICULO_SELECCION+" "+INT_TYPE+" not null,"+
                    ColumnSeleccion.TIPO_SELECCION+" "+INT_TYPE+" not null,"+
                    ColumnSeleccion.TARGET_SELECCION+" "+STRING_TYPE+" not null,"+
                    ColumnSeleccion.CUERPO_SELECCION+" "+STRING_TYPE+" not null)" ;

    public static final String INSERT_INDICE_SCRIPT =
            " insert into "+INDICE_TABLE_NAME+" values("+
                "null,0,0,\"Guia\",0)";

    public static final String INSERT_CONTENIDO_SCRIPT =
            " insert into "+CONTENIDO_TABLE_NAME+" values("+
                "null,\"1 Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. \"),"+
                "(null,\"2 Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. \"),"+
                "(null,\"3 Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. \"),"+
                "(null,\"4 Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. \"),"+
                "(null,\"5 Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. \"),"+
                "(null,\"6 Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. \")";


    private DataBaseHelper openHelper;
    private SQLiteDatabase database;

    public EsteveDataSource(Context context){
        //Se crea una instancia de la base de datos
        openHelper = new DataBaseHelper(context);
        database = openHelper.getWritableDatabase();

    }

    public void SaveIndiceRow(String titulo, Integer padre, Integer contenido){
        //Contenedor de valores
        ContentValues contenedor = new ContentValues();

        //Pasando valores al contenedor
        contenedor.put(ColumnIndice.TITULO_INDICE, titulo);
        contenedor.put(ColumnIndice.PADRE_INDICE,padre);
        contenedor.put(ColumnIndice.CONTENIDO_CUERPO, contenido);

        //Insertando en la BD

        database.insert(INDICE_TABLE_NAME,null,contenedor);

    }

    public ArrayList<Articulo> getIndicesByParent(Integer  padre){
        ArrayList<Articulo> data = new ArrayList<Articulo>();

        String columns[] = new String[]{ColumnIndice.CONTENIDO_CUERPO,ColumnIndice.TITULO_INDICE,ColumnIndice.IDENTIFICADOR_INDICE};
        String selection = ColumnIndice.PADRE_INDICE + " = ? "; //Where padre = ?
        String selectionArgs[] = new String[]{String.valueOf(padre)};

        Cursor c = database.query(
                INDICE_TABLE_NAME,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null
        );
        while (c.moveToNext()){
            Articulo item = new Articulo();

            item.setContenido(c.getString(c.getColumnIndex(ColumnIndice.CONTENIDO_CUERPO)));
            item.setTitulo(c.getString(c.getColumnIndex(ColumnIndice.TITULO_INDICE)));
            item.setId(c.getInt(c.getColumnIndex(ColumnIndice.IDENTIFICADOR_INDICE)));

            data.add(item);
        }

        return data;
    }

    public Integer getPadreID(Integer id){
        Integer padre = 0;
        String columns[] = new String[]{ColumnIndice.PADRE_INDICE};
        String selection = ColumnIndice.IDENTIFICADOR_INDICE+ " = ? "; //Where id = ?
        String selectionArgs[] = new String[]{String.valueOf(id)};

        Cursor c = database.query(
                INDICE_TABLE_NAME,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null
        );

        while (c.moveToNext()){
            padre = c.getInt(c.getColumnIndex(ColumnIndice.PADRE_INDICE));
        }
        return padre;
    }

    public String getPadreTitulo(Integer id){
        String salida = "Guía";
        String columns[] = new String[]{ColumnIndice.TITULO_INDICE};
        String selection = ColumnIndice.IDENTIFICADOR_INDICE + " = ? "; //Where id = ?
        String selectionArgs[] = new String[]{String.valueOf(id)};

        Cursor c = database.query(
                INDICE_TABLE_NAME,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null
        );

        while (c.moveToNext()){
            salida = c.getString(c.getColumnIndex(ColumnIndice.TITULO_INDICE));
        }
        return salida;
    }

    public String getContenidoArticulo(Integer id){
        String salida = "Contenido";
        String columns[] = new String[]{ColumnIndice.CONTENIDO_CUERPO};
        String selection = ColumnIndice.IDENTIFICADOR_INDICE + " = ? "; //Where id = ?
        String selectionArgs[] = new String[]{String.valueOf(id)};

        Cursor c = database.query(
                INDICE_TABLE_NAME,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null
        );

        while (c.moveToNext()){
            salida = c.getString(c.getColumnIndex(ColumnIndice.CONTENIDO_CUERPO));
        }
        return salida;
    }

    public String getTitulo(Integer id){
        String salida = "";
        String columns[] = new String[]{ColumnIndice.TITULO_INDICE};
        String selection = ColumnIndice.IDENTIFICADOR_INDICE + " = ? "; //Where id = ?
        String selectionArgs[] = new String[]{String.valueOf(id)};

        Cursor c = database.query(
                INDICE_TABLE_NAME,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null
        );

        while (c.moveToNext()){
            salida = c.getString(c.getColumnIndex(ColumnIndice.TITULO_INDICE));
        }
        return salida;
    }

    public void insertTitulo(Integer id, Integer padre, String titulo,String contenido, String created, String updated){
        String query = " insert into "+INDICE_TABLE_NAME+" values(null,"+id+","+padre+",\""+titulo+"\",\""+String.valueOf(contenido).replace('"','\'')+"\")";
        try {
            database.execSQL(query);
        }catch (SQLiteException e){
            Log.e("SQLite",e.getMessage());
            Log.e("Query",query);
        }


    }

    /**
     * Verifica si exite un articulo en la base de datos sqlite.
     * @param id Identificador del articulo a buscar
     * @return Return true en caso de que el articulo exista, false en caso contrario.
     */
    public boolean existeTitulo(Integer id){

        String columns[] = new String[]{ColumnIndice.TITULO_INDICE};
        String selection = ColumnIndice.IDENTIFICADOR_INDICE + " = ? "; //Where id = ?
        String selectionArgs[] = new String[]{String.valueOf(id)};

        Cursor c = database.query(
                INDICE_TABLE_NAME,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null
        );
        if (c.moveToNext()){
            return true;
        }
        return false;
    }


    /**
     * Verifica si un articulo tiene asociado articulos.
     * @param id Identificador del articulo.
     * @return Retorna true si el articulo tiene hijos, false en caso contrario.
     */
    public boolean hasChild(Integer id){

        String columns[] = new String[]{"count(*)"};
        String selection = ColumnIndice.PADRE_INDICE + " = ? "; //Where id = ?
        String selectionArgs[] = new String[]{String.valueOf(id)};

        Cursor c = database.query(
                INDICE_TABLE_NAME,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null
        );
        c.moveToNext();
        Log.e("DataSource","Padre: "+id+" ,Hijos: "+c.getInt(0));
       if(c.getInt(0)>0){
           return true;
       }
        return false;
    }

    /**
     * Insertar una seleccion
     * @param id identificador de la seleccion.
     * @param articulo_id Identificador del articulo.
     * @param tipo Tipo de seleccion.
     * @param target Target generado por rangy
     * @param cuerpo Comentario de la seleccion.
     */
    public void insertSelection(String id,Integer articulo_id,String tipo, String target,String cuerpo){
        String query = " insert into "+SELECCION_TABLE_NAME+" values(null,"+id+","+articulo_id+",\""+tipo+"\",\""+target+"\",\""+cuerpo+"\")";
        try {
            database.execSQL(query);
        }catch (SQLiteException e){
            Log.e("SQLite",e.getMessage());
            Log.e("Query",query);
        }
    }

    /**
     * Eliminar una seleccion de la base de datos sqlite.
     * @param id Identificador de la seleccion.
     * @param articulo Identificador del articulo.
     */
    public void deleteSelection(Integer id, Integer articulo){
        String query = "Delete from "+SELECCION_TABLE_NAME+" where "+ColumnSeleccion.ARTICULO_SELECCION+"="+articulo+" and "+ColumnSeleccion.ID_SELECCION+"="+id;
        try {
            database.execSQL(query);
        }catch (SQLiteException e){
            Log.e("SQLite",e.getMessage());
            Log.e("Query",query);
        }
    }

    /**
     * Se generera un Json con la informacion de las seleccioes
     * @param articulo Identificador del articulo al que pertenecen las notas.
     * @return Retorna JSONObject con un array de selecciones.
     */
    public JSONObject  getTargetSelection(Integer articulo){
        String salida  = "";
        JSONArray jsonArray = new JSONArray();
        JSONArray comentarios_notas = new JSONArray();
        String columns[] = new String[]{ColumnSeleccion.ID_SELECCION,ColumnSeleccion.TARGET_SELECCION,ColumnSeleccion.TIPO_SELECCION,ColumnSeleccion.CUERPO_SELECCION};
        String selection = ColumnSeleccion.ARTICULO_SELECCION + " = ? "; //Where id = ?
        String selectionArgs[] = new String[]{String.valueOf(articulo)};

        Cursor c = database.query(
                SELECCION_TABLE_NAME,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null
        );
        while (c.moveToNext()){
            String[] array = c.getString(c.getColumnIndex(ColumnSeleccion.TARGET_SELECCION)).split("[$]");
            if(array.length>1){
                if(c.getInt(c.getColumnIndex(ColumnSeleccion.TIPO_SELECCION))==1){
                    String item = array[0]+"$"+array[1]+"$"+c.getInt(c.getColumnIndex(ColumnSeleccion.ID_SELECCION))+"$highlight$";
                    salida += item;
                }else{
                    String item = array[0]+"$"+array[1]+"$"+c.getInt(c.getColumnIndex(ColumnSeleccion.ID_SELECCION))+"$note$";
                    salida += item;
                    JSONObject object  = new JSONObject();
                    try {
                        object.put("comentario",c.getString(c.getColumnIndex(ColumnSeleccion.CUERPO_SELECCION)));
                        object.put("id",c.getInt(c.getColumnIndex(ColumnSeleccion.ID_SELECCION)));
                        comentarios_notas.put(object);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if(!c.isLast()){
                    salida+="|";
                }
            }
        }
        JSONObject t = new JSONObject();
        try {
            t.put("tagerts",salida.toString());
            t.put("comentarios", comentarios_notas);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return t;
    }

    /**
     * Realizar busquedas entre el contenido de la app.
     * @param cadena palabra o fragmento de cadena a buscar
     * @return Arreglo de objestos tipo Articulo.
     */
    public ArrayList<Articulo> busqueda (String cadena){
        ArrayList<Articulo> data = new ArrayList<Articulo>();
        String columns[] = new String[]{ColumnIndice.CONTENIDO_CUERPO,ColumnIndice.TITULO_INDICE,ColumnIndice.IDENTIFICADOR_INDICE};
        String selection = ColumnIndice.CONTENIDO_CUERPO+ " like ?"; //Where padre = ?
        String selectionArgs[] = new String[]{'%'+String.valueOf(cadena)+'%'};

        Cursor c = database.query(
                INDICE_TABLE_NAME,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null
        );
        while (c.moveToNext()){
            if(!c.getString(c.getColumnIndex(ColumnIndice.TITULO_INDICE)).contains("Bibliografía")){
                if(c.getString(c.getColumnIndex(ColumnIndice.CONTENIDO_CUERPO)).length()>0){
                    Articulo item = new Articulo();

                    item.setCuerpo(c.getString(c.getColumnIndex(ColumnIndice.CONTENIDO_CUERPO)));
                    item.setTitulo(c.getString(c.getColumnIndex(ColumnIndice.TITULO_INDICE)));
                    item.setId(c.getInt(c.getColumnIndex(ColumnIndice.IDENTIFICADOR_INDICE)));

                    data.add(item);
                }

            }

        }
        return data;
    }

    /**
     * Devuelve arreglo de notas
     * @param tipo tipo de nota a seleccionar 1  para resaltado, 2 para notas.
     * @return Arreglo de objeto Nota.
     */
    public ArrayList<Nota> getSelecciones(Integer tipo){
        ArrayList<Nota> data = new ArrayList<Nota>();
        String columns[] = new String[]{ColumnSeleccion.ID_SELECCION,ColumnSeleccion.TIPO_SELECCION,ColumnSeleccion.ARTICULO_SELECCION,ColumnSeleccion.CUERPO_SELECCION};
        String selection = ColumnSeleccion.TIPO_SELECCION + " = ? "; //Where id = ?
        String selectionArgs[] = new String[]{String.valueOf(tipo)};

        Cursor c = database.query(
                SELECCION_TABLE_NAME,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null
        );

        while (c.moveToNext()){
            Nota item = new Nota();

            item.setNota(c.getString(c.getColumnIndex(ColumnSeleccion.CUERPO_SELECCION)));
            item.setIdArticulo(c.getInt(c.getColumnIndex(ColumnSeleccion.ARTICULO_SELECCION)));
            item.setId(c.getInt(c.getColumnIndex(ColumnSeleccion.ID_SELECCION)));
            item.setArticulo(getTitulo(c.getInt(c.getColumnIndex(ColumnSeleccion.ARTICULO_SELECCION))));


            data.add(item);
        }
        return data;
    }

    public void upDateSeleccion(String id, String cuerpo){
        String query = "Update "+SELECCION_TABLE_NAME+" set "+ColumnSeleccion.CUERPO_SELECCION+"='"+cuerpo+"' where "+ColumnSeleccion.ID_SELECCION+"="+id;
        try {
            database.execSQL(query);
        }catch (SQLiteException e){
            Log.e("SQLite",e.getMessage());
            Log.e("Query",query);
        }
    }




}
