package com.example.evert.myapplication2.adapters;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.example.evert.myapplication2.activities.BaseActivity;
import com.example.evert.myapplication2.R;
import com.example.evert.myapplication2.fragments.ContenidoFragment;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Evert on 24-09-2015.
 */
public class OpcionesAdapter extends BaseExpandableListAdapter{
    private Context ctx;
    private HashMap<String, List<String>> Movies_Category;
    private List<String> Movies_List;
    private int fragmentId;


    public OpcionesAdapter(Context ctx, HashMap<String, List<String>> Movies_Category, List<String> Movies_List, int fragmentId)
    {
        this.ctx = ctx;
        this.Movies_Category = Movies_Category;
        this.Movies_List = Movies_List;
        this.fragmentId = fragmentId;

    }

    @Override
    public Object getChild(int parent, int child) {


        return Movies_Category.get(Movies_List.get(parent)).get(child);
    }

    @Override
    public long getChildId(int parent, int child) {
        // TODO Auto-generated method stub
        return child;
    }

    @Override
    public View getChildView(int parent, int child, boolean lastChild, View convertview,
                             ViewGroup parentview)
    {
        final String child_title =  (String) getChild(parent, child);
        if(convertview == null)
        {
            LayoutInflater inflator = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertview = inflator.inflate(R.layout.child_layout, parentview,false);
        }
        TextView child_textview = (TextView) convertview.findViewById(R.id.child_text);
        child_textview.setText(child_title);
        child_textview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = ((Activity) ctx).getFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                Fragment fragment = new ContenidoFragment();
                Bundle args = new Bundle();
                args.putString("titulo", child_title);
                fragment.setArguments(args);
                /*for (Fragment f : BaseActivity.fragments) {
                    fragmentTransaction.hide(f);
                }*/
                BaseActivity.current_fragment_position = 4;

                ContenidoFragment ref = (ContenidoFragment) BaseActivity.fragments[4];

                ref.setTitulo(child_title);
                int screenSize = ctx.getResources().getConfiguration().screenLayout &
                        Configuration.SCREENLAYOUT_SIZE_MASK;
                if(screenSize >= Configuration.SCREENLAYOUT_SIZE_LARGE){
                    if(BaseActivity.fragments[4].isVisible()){
                        fragmentTransaction.detach(BaseActivity.fragments[4]);
                        fragmentTransaction.attach(BaseActivity.fragments[4]);
                        fragmentTransaction.commit();
                    }else{
                        fragmentTransaction.replace(R.id.contenedor_xlarge, BaseActivity.fragments[4],String.valueOf(4)).commit();
                    }
                }else{
                    if(BaseActivity.fragments[4].isVisible()){
                        fragmentTransaction.detach(BaseActivity.fragments[4]);
                        fragmentTransaction.attach(BaseActivity.fragments[4]);
                        fragmentTransaction.commit();
                    }else{
                        fragmentTransaction.replace(R.id.contenedor_principal, BaseActivity.fragments[4],String.valueOf(4)).commit();
                    }
                }



            }
        });


        return convertview;
    }

    @Override
    public int getChildrenCount(int arg0) {

        return Movies_Category.get(Movies_List.get(arg0)).size();
    }

    @Override
    public Object getGroup(int arg0) {
        // TODO Auto-generated method stub
        return Movies_List.get(arg0);
    }

    @Override
    public int getGroupCount() {
        // TODO Auto-generated method stub
        return Movies_List.size();
    }

    @Override
    public long getGroupId(int arg0) {
        // TODO Auto-generated method stub
        return arg0;
    }

    @Override
    public View getGroupView(int parent, boolean isExpanded, View convertview, ViewGroup parentview) {
        // TODO Auto-generated method stub
        String group_title = (String) getGroup(parent);
        if(convertview == null)
        {
            LayoutInflater inflator = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertview = inflator.inflate(R.layout.parent_layout, parentview,false);
        }
        TextView parent_textview = (TextView) convertview.findViewById(R.id.panrent_text);
        parent_textview.setTypeface(null, Typeface.BOLD);
        parent_textview.setText(group_title);
        return convertview;
    }

    @Override
    public boolean hasStableIds() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isChildSelectable(int arg0, int arg1) {
        // TODO Auto-generated method stub
        return false;
    }

}
