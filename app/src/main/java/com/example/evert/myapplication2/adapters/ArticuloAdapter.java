package com.example.evert.myapplication2.adapters;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.evert.myapplication2.activities.BaseActivity;
import com.example.evert.myapplication2.R;
import com.example.evert.myapplication2.fragments.ContenidoFragment;
import com.example.evert.myapplication2.models.Articulo;
import com.example.evert.myapplication2.models.Nota;

import java.util.ArrayList;

/**
 * Created by Evert on 13/11/2015.
 */
public class ArticuloAdapter extends RecyclerView.Adapter<ArticuloAdapter.ViewHolder>{
    private ArrayList<Articulo> articulos;
    private int itemLayout;


    public  ArticuloAdapter(ArrayList<Articulo> array ,int itemLayout){
        this.articulos = array;
        this.itemLayout = itemLayout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(itemLayout,viewGroup,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Articulo articulo = articulos.get(i);
        viewHolder.id.setText(String.valueOf(articulo.getId()));
        viewHolder.titulo.setText(articulo.getTitulo());
        if(articulo.getCuerpo().length()>0){
            viewHolder.cuerpo.setText(subWords(Html.fromHtml(articulo.getCuerpo()).toString(), 10) + "...");
        }

    }

    @Override
    public int getItemCount() {
        if(articulos!=null)
            return articulos.size();
        return 0;
    }

    public static class ViewHolder extends  RecyclerView.ViewHolder{
        //public TextView titulo;
        public TextView titulo;
        public TextView cuerpo;
        public TextView id;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;

            //titulo = (TextView) itemView.findViewById(R.id.titulo);
            titulo = (TextView) itemView.findViewById(R.id.articulo_titulo);
            cuerpo = (TextView) itemView.findViewById(R.id.articulo_cuerpo);
            cuerpo.setText("");
            id = (TextView) itemView.findViewById(R.id.articulo_id);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(view.getContext(), BaseActivity.class);

                    intent.putExtra("back","1");
                    intent.putExtra("articulo", id.getText());
                    intent.putExtra("titulo", titulo.getText());
                    intent.putExtra("contenido", cuerpo.getText());

                    view.getContext().startActivity(intent);
                }
            });

        }
    }

    public static String subWords(String s,Integer cantidad){

        int wordCount = 0;
        String salida = "";

        boolean word = false;
        int endOfLine = s.length() - 1;

        for (int i = 0; i < s.length(); i++) {
            // if the char is a letter, word = true.
            if (Character.isLetter(s.charAt(i)) && i != endOfLine) {
                word = true;
                // if char isn't a letter and there have been letters before,
                // counter goes up.
            } else if (!Character.isLetter(s.charAt(i)) && word) {
                wordCount++;
                word = false;
                // last word of String; if it doesn't end with a non letter, it
                // wouldn't count without this.
            } else if (Character.isLetter(s.charAt(i)) && i == endOfLine) {
                wordCount++;
            }
            if(wordCount==cantidad){
                return s.substring(0,i);
            }
        }
        return s;
    }



}
