package com.example.evert.myapplication2.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.ValueCallback;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.SearchView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.evert.myapplication2.R;
import com.example.evert.myapplication2.data.EsteveDataSource;
import com.example.evert.myapplication2.fragments.AboutUsFragment;
import com.example.evert.myapplication2.fragments.ComiteFragment;
import com.example.evert.myapplication2.fragments.ContenidoFragment;
import com.example.evert.myapplication2.fragments.CreditosFragment;
import com.example.evert.myapplication2.fragments.GuiaFragment;
import com.example.evert.myapplication2.fragments.IndiceFragment;
import com.example.evert.myapplication2.fragments.IntroduccionFragment;
import com.example.evert.myapplication2.fragments.NotasFragment;
import com.example.evert.myapplication2.fragments.PrologoFragment;
import com.example.evert.myapplication2.fragments.TablaFragment;
import com.example.evert.myapplication2.models.Articulo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class BaseActivity extends Activity {
    public static DrawerLayout mDrawerLayout;
    public static ListView mDrawerList;
    static final String FRAGMENT_ACTUAL = "fragment_actual";
    private ActionBarDrawerToggle mDrawerToggle;
    Integer item_padre;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private EsteveDataSource dataSource;
    private ArrayList<Articulo> articulos;

    public static int current_fragment_position = 0;
    private Fragment[] fragments_acerca_de = new Fragment[]{
            new ComiteFragment(),
            new IntroduccionFragment(),
            new PrologoFragment(),
            new CreditosFragment(),

    };
    public Boolean bandera_back = true;
    public static FragmentManager manager;
    public static FragmentTransaction fragmentTransaction;
    public static String[] TitulosOpcion;
    public static Fragment[] fragments = new Fragment[]{
            new IndiceFragment(),    //Indice primario
            new TablaFragment(),     //Tabla de interecciones
            new NotasFragment(),     //Listado de notas
            new AboutUsFragment(),   //Fragmente acerca de
            new ContenidoFragment(), //Contenido de articulo
            new GuiaFragment()       //Indice secundario
    };

    private ActionMode mActionMode = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        item_padre = 1;
        int screenSize = this.getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;

        mTitle = mDrawerTitle = getTitle();
        TitulosOpcion = getResources().getStringArray(R.array.opciones_nav);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener
        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, TitulosOpcion));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // enable ActionBar app icon to behave as action to toggle nav drawer
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);


        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(mTitle);
            }

            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(mDrawerTitle);
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        dataSource = new  EsteveDataSource(this);


        // Se recibe el intent, para saber desde donde se llamó la actividad
        Intent intent = getIntent();
        if(getIntent().hasExtra("back")){
            switch (Integer.valueOf(getIntent().getStringExtra("back"))){
                case 1:
                case 2:
                    Log.e("BaseActivity","Visualizando fragment de contenido");
                    //Se visualiza el fragment de contenido
                    manager = getFragmentManager();
                    fragmentTransaction = manager.beginTransaction();
                    ContenidoFragment contenidoFragment = new ContenidoFragment();
                    Bundle b = new Bundle();
                    b.putString("titulo",getIntent().getStringExtra("titulo"));
                    b.putString("contenido", getIntent().getStringExtra("contenido"));
                    b.putInt("IdContenido", Integer.valueOf(getIntent().getStringExtra("articulo")));
                    contenidoFragment.setArguments(b);
                    if(screenSize >= Configuration.SCREENLAYOUT_SIZE_LARGE) {
                        fragmentTransaction.replace(R.id.contenedor_xlarge, contenidoFragment, String.valueOf(4)).commit();
                    }else{
                        fragmentTransaction.replace(R.id.contenedor_principal, contenidoFragment, String.valueOf(4)).commit();
                    }


                    mDrawerList.setItemChecked(0, true);
                    break;
                case 0:
                    selectItem(0);
                    break;



            }
        }else{
            if (savedInstanceState != null) {

                if (this.current_fragment_position != 0) {

                    if(screenSize >= Configuration.SCREENLAYOUT_SIZE_LARGE) {
                        manager = getFragmentManager();
                        fragmentTransaction = manager.beginTransaction();
                        fragmentTransaction.replace(R.id.contenedor_xlarge, fragments[4], String.valueOf(4)).commit();
                        mDrawerList.setItemChecked(0, true);
                        this.current_fragment_position = 0;
                    }else{
                        manager = getFragmentManager();
                        fragmentTransaction = manager.beginTransaction();
                        fragmentTransaction.replace(R.id.contenedor_principal, fragments[4], String.valueOf(4)).commit();
                        mDrawerList.setItemChecked(0, true);
                        this.current_fragment_position = 0;
                    }


                } else {
                    selectItem(0);
                }
            } else {
                /*selectItem(4);
                String url = "http://esteve.medericediciones.com/contenido/";
                RequestQueue queue = Volley.newRequestQueue(this);
                final ProgressDialog progressDialog = ProgressDialog.show(this, "Espere por favor.", "Estamos actualizando el contenido.");
                JsonArrayRequest req = new JsonArrayRequest(url,new Response.Listener<JSONArray>(){

                    @Override
                    public void onResponse(JSONArray response) {
                        Log.e("Request",response.toString());
                        articulos = parseJson(response);
                        for (int i=0; i<articulos.size();i++){
                            if(!dataSource.existeTitulo(articulos.get(i).getId()))
                                dataSource.insertTitulo(articulos.get(i).getId(),articulos.get(i).getPadre(),articulos.get(i).getTitulo(),articulos.get(i).getContenido(),"","");
                        }
                        Bundle b  = new Bundle();
                        b.putInt("padre", 0);
                        IndiceFragment indice  = new IndiceFragment();
                        GuiaFragment guia = new GuiaFragment();
                        indice.setArguments(b);
                        guia.setArguments(b);

                        fragments[0] =  indice;
                        fragments[5] = guia;
                        selectItem(0);
                        progressDialog.cancel();
                    }
                },new Response.ErrorListener(){

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Volley","R: "+error.getMessage());
                        progressDialog.cancel();
                        selectItem(0);
                    }
                });
                queue.add(req);
                Log.e("Base Activity", "Actualizando fragmen del indice.");

                this.current_fragment_position = 0;
                */

                Log.e("BaseActivity","Visualizando fragment de contenido");
                //Se visualiza el fragment de contenido
                manager = getFragmentManager();
                fragmentTransaction = manager.beginTransaction();
                ContenidoFragment contenidoFragment = new ContenidoFragment();
                /*Bundle b = new Bundle();
                b.putString("titulo",getIntent().getStringExtra("titulo"));
                b.putString("contenido", getIntent().getStringExtra("contenido"));
                b.putInt("IdContenido", Integer.valueOf(getIntent().getStringExtra("articulo")));
                contenidoFragment.setArguments(b);
                */
                if(screenSize >= Configuration.SCREENLAYOUT_SIZE_LARGE) {
                    fragmentTransaction.replace(R.id.contenedor_xlarge, contenidoFragment, String.valueOf(4)).commit();
                }else{
                    fragmentTransaction.replace(R.id.contenedor_principal, contenidoFragment, String.valueOf(4)).commit();
                }


                mDrawerList.setItemChecked(0, true);

            }

        }


        /*if(getIntent().hasExtra("back")  && Integer.valueOf(getIntent().getStringExtra("back"))==1){ //Si la actividad se llamó desde alguna busqueda
            Log.e("BaseActivity","Visualizando fragment de contenido");
            //Se visualiza el fragment de contenido
            manager = getFragmentManager();
            fragmentTransaction = manager.beginTransaction();
            ContenidoFragment contenidoFragment = new ContenidoFragment();
            Bundle b = new Bundle();
            b.putString("titulo",getIntent().getStringExtra("titulo"));
            b.putString("contenido", getIntent().getStringExtra("contenido"));
            b.putInt("IdContenido", Integer.valueOf(getIntent().getStringExtra("articulo")));
            contenidoFragment.setArguments(b);
            if(screenSize >= Configuration.SCREENLAYOUT_SIZE_LARGE) {
                fragmentTransaction.replace(R.id.contenedor_xlarge, contenidoFragment, String.valueOf(4)).commit();
            }else{
                fragmentTransaction.replace(R.id.contenedor_principal, contenidoFragment, String.valueOf(4)).commit();
            }
            bandera_back = true;

            mDrawerList.setItemChecked(0, true);


        }else {
            if(getIntent().hasExtra("back")  && Integer.valueOf(getIntent().getStringExtra("back"))==0){
                selectItem(0);
            }else {
                if (savedInstanceState != null) {

                    if (this.current_fragment_position != 0) {

                        if(screenSize >= Configuration.SCREENLAYOUT_SIZE_LARGE) {
                            manager = getFragmentManager();
                            fragmentTransaction = manager.beginTransaction();
                            fragmentTransaction.replace(R.id.contenedor_xlarge, fragments[4], String.valueOf(4)).commit();
                            mDrawerList.setItemChecked(0, true);
                            this.current_fragment_position = 0;
                        }else{
                            manager = getFragmentManager();
                            fragmentTransaction = manager.beginTransaction();
                            fragmentTransaction.replace(R.id.contenedor_principal, fragments[4], String.valueOf(4)).commit();
                            mDrawerList.setItemChecked(0, true);
                            this.current_fragment_position = 0;
                        }


                    } else {
                        selectItem(0);
                    }
                } else {
                    String url = "http://esteve.medericediciones.com/contenido/";
                    RequestQueue queue = Volley.newRequestQueue(this);
                    final ProgressDialog progressDialog = ProgressDialog.show(this, "Espere por favor.", "Estamos actualizando el contenido.");
                    JsonArrayRequest req = new JsonArrayRequest(url,new Response.Listener<JSONArray>(){

                        @Override
                        public void onResponse(JSONArray response) {
                            Log.e("Request",response.toString());
                            articulos = parseJson(response);
                            for (int i=0; i<articulos.size();i++){
                                if(!dataSource.existeTitulo(articulos.get(i).getId()))
                                    dataSource.insertTitulo(articulos.get(i).getId(),articulos.get(i).getPadre(),articulos.get(i).getTitulo(),articulos.get(i).getContenido(),"","");
                            }
                            Bundle b  = new Bundle();
                            b.putInt("padre", 0);
                            IndiceFragment indice  = new IndiceFragment();
                            GuiaFragment guia = new GuiaFragment();
                            indice.setArguments(b);
                            guia.setArguments(b);

                            fragments[0] =  indice;
                            fragments[5] = guia;
                            selectItem(0);
                            progressDialog.cancel();
                        }
                    },new Response.ErrorListener(){

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("Volley","R: "+error.getMessage());
                            progressDialog.cancel();
                        }
                    });
                    queue.add(req);
                    Log.e("Base Activity", "Actualizando fragmen del indice.");

                    this.current_fragment_position = 0;

                }
            }
        }*/


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_websearch)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));

        return super.onCreateOptionsMenu(menu);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action buttons
        switch(item.getItemId()) {
            case R.id.action_websearch:
                // create intent to perform web search for this planet
                Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
                intent.putExtra(SearchManager.QUERY, getActionBar().getTitle());
                // catch event that there's no activity to handle intent
                if (intent.resolveActivity(getPackageManager()) != null) {
                    //startActivity(intent);
                } else {
                    Toast.makeText(this, R.string.app_not_available, Toast.LENGTH_LONG).show();
                }
                return true;
        }
        return false;
    }



    /* The click listner for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
             selectItem(position);
        }
    }

    private void selectItem(int position) {
        // update the main content by replacing fragments
        Log.d("BaseActivity","Position: "+String.valueOf(position));
        manager = getFragmentManager();
        fragmentTransaction = manager.beginTransaction();
        switch (position){
            case 3:
                Intent intent = new Intent(this,AcercaActivity.class);
                startActivity(intent);
                break;
            case 1:
                Intent intent_tabla = new Intent(this,TablaActivity.class);
                startActivity(intent_tabla);
                break;
            case 2:
                Intent intent_notas = new Intent(this,NotasActivity.class);
                startActivity(intent_notas);
                break;
            case 0:
                Bundle b  = new Bundle();
                b.putInt("padre", 0);
                IndiceFragment indice  = new IndiceFragment();
                indice.setArguments(b);
                fragmentTransaction.replace(R.id.contenedor_principal, indice, String.valueOf(position)).commit();
                setTitle(TitulosOpcion[position]);
                break;
            default:
                    if(fragments[position].isVisible()){
                        fragmentTransaction.detach(fragments[position]);
                        fragmentTransaction.attach(fragments[position]);
                        fragmentTransaction.commit();
                    }else {
                        fragmentTransaction.replace(R.id.contenedor_principal, fragments[position], String.valueOf(position)).commit();
                    }
                    setTitle(TitulosOpcion[position]);
                break;

        }

        mDrawerList.setItemChecked(position, true);
        mDrawerLayout.closeDrawer(mDrawerList);

    }

    public void setPadreItem(int padre){
        this.item_padre = padre;
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);

    }

    public void onClickMenu(View view){
        PopupMenu popup = new PopupMenu(this, view);
        ArrayList<Articulo> articulos = dataSource.getIndicesByParent(0);
        for (int i = 0; i < articulos.size();i++){

            popup.getMenu().add(0,articulos.get(i).getId(),0,articulos.get(i).getTitulo());
        }
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(android.view.MenuItem item) {

                getActionBar().setTitle(item.getTitle());

                manager = getFragmentManager();
                fragmentTransaction = manager.beginTransaction();
                GuiaFragment guiaFragment = new GuiaFragment();
                Bundle b = new Bundle();
                item_padre = item.getItemId();
                Log.d("OnMenuClick", "Configurado item padre a: " + String.valueOf(item.getItemId()));
                b.putInt("padre", item.getItemId());
                guiaFragment.setArguments(b);
                fragmentTransaction.replace(R.id.contenedor_principal, guiaFragment, String.valueOf(5)).commit();
                return true;
            }
        });
        popup.show();
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
        //getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        int screenSize = this.getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            FragmentManager fm = this.manager;

            if(getIntent().hasExtra("back")) { //Si la actividad se llamó desde alguna busqueda
                if( Integer.valueOf(getIntent().getStringExtra("back"))==1){
                    Intent intent = new Intent(this,SearchResultsActivity.class);
                    //Se redirige a la actividad de resultados.

                    startActivity(intent);
                }else{
                    Intent intent = new Intent(this,NotasActivity.class);
                    //Se redirige a la actividad de resultados.

                    startActivity(intent);
                }


            }else {



                    if ((this.item_padre == 0 && screenSize >= Configuration.SCREENLAYOUT_SIZE_LARGE) || (this.item_padre < 1 && screenSize < Configuration.SCREENLAYOUT_SIZE_LARGE) ) {
                        //Si el indice primario esta seleccionado, es porque se desea salir de la app
                        final AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
                        builder1.setMessage("¿Desea salir de la aplicación?");
                        builder1.setCancelable(true);
                        builder1.setPositiveButton("Si",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        BaseActivity.this.finish();
                                        dialog.cancel();
                                    }
                                });
                        builder1.setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    } else {

                        if(screenSize >= Configuration.SCREENLAYOUT_SIZE_LARGE) {
                            fragmentTransaction = manager.beginTransaction();
                            GuiaFragment guiaFragment = new GuiaFragment();
                            Bundle b = new Bundle();
                            b.putInt("padre", this.dataSource.getPadreID(this.item_padre));
                            b.putString("titulo",this.dataSource.getPadreTitulo(this.item_padre));
                            guiaFragment.setArguments(b);
                            this.item_padre = this.dataSource.getPadreID(this.item_padre);
                            fragmentTransaction.replace(R.id.contenedor_principal, guiaFragment, String.valueOf(5)).commit();

                        }else{
                            fragmentTransaction = manager.beginTransaction();
                            GuiaFragment guiaFragment = new GuiaFragment();
                            Bundle b = new Bundle();
                            this.item_padre = this.dataSource.getPadreID(this.item_padre);
                            b.putInt("padre", item_padre);
                            b.putString("titulo",this.dataSource.getPadreTitulo(this.item_padre));
                            guiaFragment.setArguments(b);
                            fragmentTransaction.replace(R.id.contenedor_principal, guiaFragment, String.valueOf(5)).commit();
                        }
                    }

            }

        }
        return false;
    }

    public ArrayList<Articulo> parseJson(JSONArray response){
        ArrayList<Articulo> articulos = new ArrayList<Articulo>();
        for (int i= 0; i<response.length(); i++){
            Articulo articulo = new Articulo();
            try{
                JSONObject jsonObject = (JSONObject) response.get(i);
                if(jsonObject.getString("parent_id") == "null"){
                    articulo.setPadre(0);
                }else{
                    articulo.setPadre(jsonObject.getInt("parent_id"));
                }
                articulo.setId(jsonObject.getInt("id"));
                articulo.setTitulo(jsonObject.getString("title"));
                articulo.setContenido(jsonObject.getString("topContent"));
                articulo.setCuerpo(jsonObject.getString("topContent"));


                articulos.add(articulo);

            }catch (JSONException e){
                Log.e("Error JSON",e.getMessage());
            }
        }
        return articulos;
    }

    @Override
    public void onActionModeStarted(ActionMode mode) {
        if (mActionMode == null) {
            mActionMode = mode;
            Menu menu = mode.getMenu();
            // Remove the default menu items (select all, copy, paste, search)
            menu.clear();

            // If you want to keep any of the defaults,
            // remove the items you don't want individually:
            // menu.removeItem(android.R.id.[id_of_item_to_remove])

            // Inflate your own menu items
            mode.getMenuInflater().inflate(R.menu.style, menu);
            menu.findItem(R.id.resaltar).setOnMenuItemClickListener(new EventMenuItemListener(this, mode));
            menu.findItem(R.id.nota).setOnMenuItemClickListener(new EventMenuItemListener(this, mode));
            menu.findItem(R.id.delete).setOnMenuItemClickListener(new EventMenuItemListener(this, mode));
        }

        super.onActionModeStarted(mode);
    }



    @Override
    public void onActionModeFinished(ActionMode mode) {
        mActionMode = null;
        super.onActionModeFinished(mode);
    }

    private class EventMenuItemListener implements MenuItem.OnMenuItemClickListener {

        private final Context context;
        private final ActionMode actionMode;

        private EventMenuItemListener(Context context, ActionMode actionMode) {
            this.context = context;
            this.actionMode = actionMode;
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            manager = getFragmentManager();
            ContenidoFragment contenidoFragment = (ContenidoFragment)  manager.findFragmentByTag("4");
            if(contenidoFragment!=null && contenidoFragment.isVisible()){
                switch (item.getItemId()) {
                    case R.id.resaltar:
                        contenidoFragment.resaltarContenido();

                        if (mActionMode != null) {
                            mActionMode.finish();
                        }
                        //mTextSelectionSupport.endSelectionMode();
                        break;
                    case R.id.nota:
                        // do some different stuff
                        contenidoFragment.realizarNota(mActionMode);
                        break;
                    case R.id.delete:
                        contenidoFragment.eliminarSeleccion();

                        if (mActionMode != null) {
                            mActionMode.finish();
                        }
                        //mTextSelectionSupport.endSelectionMode();
                        break;
                }

            }
            return true;
        }
    }

}