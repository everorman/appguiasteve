package com.example.evert.myapplication2.adapters;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.example.evert.myapplication2.R;
import com.example.evert.myapplication2.models.Articulo;

import java.util.List;

/**
 * Created by Evert on 10/11/2015.
 */
public class ItemAdapter extends ArrayAdapter<Articulo> {

    public ItemAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public ItemAdapter(Context context, int resource, List<Articulo> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.item_list_with_icon_right, null);
        }

        Articulo p = getItem(position);

        if (p != null) {

            TextView titulo = (TextView) v.findViewById(R.id.titulo_indice); // Referencia al text view del layout
            titulo.setText(p.getTitulo());
        }

        return v;
    }

}
