package com.example.evert.myapplication2.models;

/**
 * Created by Evert on 25-09-2015.
 */
public class Nota {
    private String nombre;
    private String nota;
    private String articulo;
    private Integer id;
    private Integer idArticulo;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNota() {return nota;}

    public void setNota(String nota) {
        this.nota = nota;
    }

    public String getArticulo() {
        return articulo;
    }

    public String getNot() {
        return nota;
    }

    public void setArticulo(String articulo) {
        this.articulo = articulo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdArticulo() {
        return idArticulo;
    }

    public void setIdArticulo(Integer id_articulo) {
        this.idArticulo = id_articulo;
    }
}
