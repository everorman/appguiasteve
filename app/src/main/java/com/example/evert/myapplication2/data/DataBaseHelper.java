package com.example.evert.myapplication2.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Evert on 2/12/2015.
 */
public class DataBaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "esteve.db";
    public static final int DATABASE_VERSION = 1;

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //Crear la tabla Indice
        db.execSQL(EsteveDataSource.CREATE_INDICE_SCRIPT);
        db.execSQL(EsteveDataSource.CREATE_SELECCION_SCRIPT);
        //db.execSQL(EsteveDataSource.CREATE_CONTENIDO_SCRIPT);
        //Insertar datos iniciales
        //db.execSQL(EsteveDataSource.INSERT_INDICE_SCRIPT);
        //db.execSQL(EsteveDataSource.INSERT_CONTENIDO_SCRIPT);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
