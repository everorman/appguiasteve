package com.example.evert.myapplication2.models;

import android.util.Log;

import com.example.evert.myapplication2.data.EsteveDataSource;

/**
 * Created by Evert on 13/11/2015.
 */
public class Articulo {

    private String titulo;
    private String cuerpo;
    private int id;
    private String contenido;
    private int padre;



    public Articulo(String titulo, String cuerpo, int id) {
        this.titulo = titulo;
        this.cuerpo = cuerpo;
        this.id = id;
        this.padre = 0;
        this.contenido = "";

    }

    public Articulo() {
        this.titulo = null;
        this.cuerpo = null;
        this.id = 0;
        this.contenido = "";
        this.padre = 0;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public int getPadre() {
        return padre;
    }

    public void setPadre(int padre) {
        this.padre = padre;
    }

    public boolean hasContent(){
        Log.e("ArticuloClass", this.getContenido());
        if(this.getContenido().length()>0)
            return  true;
        return false;
    }

    public boolean hasChild(EsteveDataSource dataSource){
        Boolean salida = dataSource.hasChild(this.getId());
        Log.e("HasChild",String.valueOf(salida)+" Padre: "+this.getId());
        return  salida;
    }
}
