package com.example.evert.myapplication2.fragments;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.content.res.Configuration;
import android.os.Bundle;


import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.example.evert.myapplication2.activities.BaseActivity;
import com.example.evert.myapplication2.R;
import com.example.evert.myapplication2.adapters.ItemAdapter;
import com.example.evert.myapplication2.data.EsteveDataSource;
import com.example.evert.myapplication2.models.Articulo;

import java.util.ArrayList;
import java.util.Arrays;

public class IndiceFragment extends ListFragment {
    private ArrayList<Articulo> items;
    private View root;
    private EsteveDataSource dataSource;
    public IndiceFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String[] items_2 = {
                this.getActivity().getString(R.string.opcion_1),
                this.getActivity().getString(R.string.opcion_2),
                this.getActivity().getString(R.string.opcion_3),
                this.getActivity().getString(R.string.opcion_4),
                this.getActivity().getString(R.string.opcion_5),
                this.getActivity().getString(R.string.opcion_6),
                this.getActivity().getString(R.string.opcion_7),
                this.getActivity().getString(R.string.opcion_8),
                this.getActivity().getString(R.string.opcion_9),
                this.getActivity().getString(R.string.opcion_10),
                this.getActivity().getString(R.string.opcion_11),
                this.getActivity().getString(R.string.opcion_12),
        };
        //items = items_2;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_indice, container, false);
        dataSource = new  EsteveDataSource(this.root.getContext());
        items = dataSource.getIndicesByParent(1);
        Bundle bundle=getArguments();
        if(bundle!=null){
            if(bundle.containsKey("padre")){
                Integer id_padre = bundle.getInt("padre");
                //String titulo_padre = bundle.getString("titulo");
                items = dataSource.getIndicesByParent(id_padre);
                Log.e("GuiaFragment",String.valueOf(bundle.getInt("padre")) );
            }else{
                Log.e("GuiaFragment","No se recibió id de padre, por lo que se selecciona la raiz. Padre = 0" );
                items = dataSource.getIndicesByParent(0);
            }
            if(bundle.containsKey("titulo")){
                getActivity().getActionBar().setTitle(bundle.getString("titulo"));
            }


        }else{
            Log.e("GuiaFragment","No se recibió id de padre, por lo que se selecciona la raiz. Padre = 0" );
            items = dataSource.getIndicesByParent(0);
        }
        setListAdapter(new ItemAdapter(this.root.getContext(), R.layout.item_list_with_icon_right, items));
        return root;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        // TODO Auto-generated method stub
        super.onListItemClick(l, v, position, id);
        Log.d("IndiceFragment", "Preparado para capturar evento.");
        Log.d("IndiceFragment","PositionVar: "+String.valueOf(position));
        FragmentManager m = getFragmentManager();
        FragmentTransaction ft = m.beginTransaction();
        Articulo item = new Articulo();
        item = items.get(position);
        if(item.hasChild(dataSource)){
            Bundle b = new Bundle();
            ft = m.beginTransaction();
            ((GuiaFragment) BaseActivity.fragments[5]).setTitulo(item.getTitulo());
            GuiaFragment guiaFragment = new GuiaFragment();
            b.putInt("padre",item.getId());
            b.putString("titulo", item.getTitulo());
            guiaFragment.setArguments(b);
            ((BaseActivity)getActivity()).setPadreItem(item.getId());
            ft.replace(R.id.contenedor_principal, guiaFragment ,String.valueOf(5)).commit();
        }
        if(item.hasContent()){
            ft = m.beginTransaction();
            ContenidoFragment contenidoFragment = new ContenidoFragment();
            Bundle b = new Bundle();
            b.putString("titulo",item.getTitulo());
            b.putString("contenido", item.getContenido());
            b.putInt("IdContenido",item.getId());
            contenidoFragment.setArguments(b);
            ((BaseActivity)getActivity()).setPadreItem(item.getId());
            int screenSize = this.getResources().getConfiguration().screenLayout &
                    Configuration.SCREENLAYOUT_SIZE_MASK;
            if(screenSize >= Configuration.SCREENLAYOUT_SIZE_LARGE) {
                ft.replace(R.id.contenedor_xlarge, contenidoFragment ,String.valueOf(4)).commit();
            }else{
                ft.replace(R.id.contenedor_principal, contenidoFragment ,String.valueOf(4)).commit();
            }


        }else{
            ft = m.beginTransaction();
            ((GuiaFragment) BaseActivity.fragments[5]).setTitulo(item.getTitulo());
            GuiaFragment guiaFragment = new GuiaFragment();
            Bundle b = new Bundle();
            b.putInt("padre",item.getId());
            b.putString("titulo", item.getTitulo());
            guiaFragment.setArguments(b);
            ((BaseActivity)getActivity()).setPadreItem(item.getId());
            Log.e("ItemFragmen ItemClick","Padre : "+String.valueOf(item.getId()));
            ft.replace(R.id.contenedor_principal, guiaFragment ,String.valueOf(5)).commit();
        }

        //Toast.makeText(root.getContext(), toastMsg, Toast.LENGTH_LONG).show();
    }

}
