package com.example.evert.myapplication2.fragments;

import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.evert.myapplication2.R;
import com.example.evert.myapplication2.adapters.NotasAdapter;
import com.example.evert.myapplication2.models.Nota;

import java.util.ArrayList;

public class NotasFragment extends Fragment{
    public static final String TAG_NAME = "NOTAS_FRAGMENT";
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public NotasFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notas, container, false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        ArrayList<Nota> notaArrayList = new ArrayList<>();

        Nota nota_1 = new Nota();
        nota_1.setArticulo("Articulo de prueba");
        nota_1.setNombre("Nota");
        nota_1.setNota("Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.");
        notaArrayList.add(nota_1);
        RecyclerView recyclerView = (RecyclerView) getActivity().findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(new NotasAdapter(notaArrayList, R.layout.row_notas));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

}
