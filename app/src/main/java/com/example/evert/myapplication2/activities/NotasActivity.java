package com.example.evert.myapplication2.activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;

import com.example.evert.myapplication2.R;
import com.example.evert.myapplication2.adapters.NotasAdapter;
import com.example.evert.myapplication2.data.EsteveDataSource;
import com.example.evert.myapplication2.fragments.GuiaFragment;
import com.example.evert.myapplication2.models.Nota;

import java.util.ArrayList;

public class NotasActivity extends Activity {
    private EsteveDataSource dataSource;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dataSource = new EsteveDataSource(this);
        setContentView(R.layout.activity_notas);

        ArrayList<Nota> notaArrayList = dataSource.getSelecciones(2);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(new NotasAdapter(notaArrayList, R.layout.row_notas));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent(this,BaseActivity.class);
            startActivity(intent);
        }

        return super.onKeyDown(keyCode, event);
    }
}
