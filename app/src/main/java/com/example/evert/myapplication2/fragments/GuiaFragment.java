package com.example.evert.myapplication2.fragments;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.content.res.Configuration;
import android.graphics.Outline;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.example.evert.myapplication2.activities.BaseActivity;
import com.example.evert.myapplication2.adapters.ItemAdapter;
import com.example.evert.myapplication2.data.DataProvider;
import com.example.evert.myapplication2.adapters.OpcionesAdapter;
import com.example.evert.myapplication2.R;
import com.example.evert.myapplication2.data.EsteveDataSource;
import com.example.evert.myapplication2.models.Articulo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class GuiaFragment extends ListFragment {
/**
 * Fragment that appears in the "content_frame", shows a planet
 */
    HashMap<String, List<String>> Movies_category;
    List<String> Movies_list;
    ExpandableListView Exp_list;
    OpcionesAdapter adapter;


    /*Declaracion 2do contenedor*/
    HashMap<String, List<String>> DataItem2;
    List<String> Item2List;
    ExpandableListView ExpList2;


    private ArrayList<Articulo> items;
    private EsteveDataSource dataSource;
    String titulo;

    public static final String TAG_NAME = "GUIA_FRAGMENT";

    private View root;

    public GuiaFragment() {
        // Empty constructor required for fragment subclasses
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_indice,container,false);
        /*int size = getResources().getDimensionPixelSize(R.dimen.fab_size);
        Outline outline = new Outline();
        outline.setOval(0, 0, size, size);
        ImageButton imageButton = (ImageButton)  root.findViewById(R.id.imageButton);
        ViewOutlineProvider viewOutlineProvider = new ViewOutlineProvider() {
            @Override
            public void getOutline(View view, Outline outline) {
                // Or read size directly from the view's width/height
                int size = getResources().getDimensionPixelSize(R.dimen.fab_size);
                outline.setOval(0, 0, size, size);
            }
        };*/


        /*Configuración del titulo 1*/
        /*Exp_list = (ExpandableListView) root.findViewById(R.id.listItem1);
        Movies_category = DataProvider.getItem1();
        Movies_list = new ArrayList<String>(Movies_category.keySet());

        adapter = new OpcionesAdapter(root.getContext(), Movies_category, Movies_list,this.getId());
        Exp_list.setAdapter(adapter);*/



        getActivity().getActionBar().setTitle(this.titulo);
        dataSource = new  EsteveDataSource(this.root.getContext());

        Bundle bundle=getArguments();
        if(bundle!=null){
            if(bundle.containsKey("padre")){
                Integer id_padre = bundle.getInt("padre");
                //String titulo_padre = bundle.getString("titulo");
                Log.e("GuiaFragment",String.valueOf(bundle.getInt("padre")) );
                items = dataSource.getIndicesByParent(id_padre);
            }else{
                Log.e("GuiaFragment","No se recibió id de padre, por lo que se selecciona la raiz. Padre = 0" );
                items = dataSource.getIndicesByParent(0);
            }
            if(bundle.containsKey("titulo")){
                getActivity().getActionBar().setTitle(bundle.getString("titulo"));
            }


        }

        setListAdapter(new ItemAdapter(this.root.getContext(), R.layout.item_list_with_icon_right, items));
        return root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action buttons
        switch(item.getItemId()) {
            case R.id.opcion_1:
                Toast.makeText(this.root.getContext(), "Opcion " + item.getItemId(),Toast.LENGTH_SHORT).show();
                Log.e("Item menu", "click");
                return true;
            case R.id.opcion_2:
                Toast.makeText(this.root.getContext(), "Opcion " + item.getItemId(),Toast.LENGTH_SHORT).show();
                Log.e("Item menu", "click");
                return true;
            case R.id.opcion_3:
                Toast.makeText(this.root.getContext(), "Opcion " + item.getItemId(),Toast.LENGTH_SHORT).show();
                Log.e("Item menu", "click");
                return true;
            case R.id.opcion_4:
                Toast.makeText(this.root.getContext(), "Opcion " + item.getItemId(),Toast.LENGTH_SHORT).show();
                Log.e("Item menu", "click");
                return true;
            case R.id.opcion_5:
                Toast.makeText(this.root.getContext(), "Opcion " + item.getItemId(),Toast.LENGTH_SHORT).show();
                Log.e("Item menu", "click");
                return true;
            case R.id.opcion_6:
                Toast.makeText(this.root.getContext(), "Opcion " + item.getItemId(),Toast.LENGTH_SHORT).show();
                Log.e("Item menu", "click");
                return true;
            case R.id.opcion_7:
                Toast.makeText(this.root.getContext(), "Opcion " + item.getItemId(),Toast.LENGTH_SHORT).show();
                Log.e("Item menu", "click");
                return true;
            case R.id.opcion_8:
                Toast.makeText(this.root.getContext(), "Opcion " + item.getItemId(),Toast.LENGTH_SHORT).show();
                Log.e("Item menu", "click");
                return true;
            case R.id.opcion_9:
                Toast.makeText(this.root.getContext(), "Opcion " + item.getItemId(),Toast.LENGTH_SHORT).show();
                Log.e("Item menu", "click");
                return true;
            case R.id.opcion_10:
                Toast.makeText(this.root.getContext(), "Opcion " + item.getItemId(),Toast.LENGTH_SHORT).show();
                Log.e("Item menu", "click");
                return true;
            case R.id.opcion_11:
                Toast.makeText(this.root.getContext(), "Opcion " + item.getItemId(),Toast.LENGTH_SHORT).show();
                Log.e("Item menu", "click");
                return true;
            case R.id.opcion_12:
                Toast.makeText(this.root.getContext(), "Opcion " + item.getItemId(),Toast.LENGTH_SHORT).show();
                Log.e("Item menu", "click");
                return true;


        }
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public void setTitulo(String titulo){
        this.titulo = titulo ;

    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        // TODO Auto-generated method stub
        super.onListItemClick(l, v, position, id);
        Log.d("GuiaFragment", "Preparado para capturar evento.");
        Log.d("GuiaFragment","PositionVar: "+String.valueOf(position));
        FragmentManager m = getFragmentManager();
        FragmentTransaction ft = m.beginTransaction();
        Articulo item = items.get(position);


        // Mostramos un mensaje con el elemento pulsado
        //Toast.makeText(getActivity(), "Ha pulsado " + items[position],Toast.LENGTH_SHORT).show();
        if(item.hasChild(dataSource)){
            Bundle b = new Bundle();
            ft = m.beginTransaction();
            ((GuiaFragment) BaseActivity.fragments[5]).setTitulo(item.getTitulo());
            IndiceFragment indiceFragment = new IndiceFragment();
            Log.e("ItemID","Item ID: "+ item.getId());
            b.putInt("padre",item.getId());
            b.putString("titulo", item.getTitulo());
            indiceFragment.setArguments(b);
            ((BaseActivity)getActivity()).setPadreItem(item.getId());
            ft.replace(R.id.contenedor_principal, indiceFragment ,String.valueOf(5)).commit();
        }
        if(item.hasContent()){
            ft = m.beginTransaction();
            ContenidoFragment contenidoFragment = new ContenidoFragment();
            Bundle b = new Bundle();
            b.putString("titulo",item.getTitulo());
            b.putString("contenido",item.getContenido());
            b.putInt("IdContenido",item.getId());
            contenidoFragment.setArguments(b);
            ((BaseActivity)getActivity()).setPadreItem(item.getId());
            int screenSize = this.getResources().getConfiguration().screenLayout &
                    Configuration.SCREENLAYOUT_SIZE_MASK;
            if(screenSize >= Configuration.SCREENLAYOUT_SIZE_LARGE) {
                ft.replace(R.id.contenedor_xlarge, contenidoFragment ,String.valueOf(4)).commit();
            }else{
                ft.replace(R.id.contenedor_principal, contenidoFragment ,String.valueOf(4)).commit();
            }

        }else{
            ft = m.beginTransaction();
            ((GuiaFragment) BaseActivity.fragments[5]).setTitulo(item.getTitulo());
            GuiaFragment guiaFragment = new GuiaFragment();
            Bundle b = new Bundle();
            b.putInt("padre", item.getId());
            b.putString("titulo", item.getTitulo());
            guiaFragment.setArguments(b);
            ((BaseActivity)getActivity()).setPadreItem(item.getId());
            ft.replace(R.id.contenedor_principal, guiaFragment ,String.valueOf(5)).commit();
        }


        //Toast.makeText(root.getContext(), toastMsg, Toast.LENGTH_LONG).show();
    }




}

