package com.example.evert.myapplication2.fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.style.BackgroundColorSpan;
import android.text.style.CharacterStyle;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import com.example.evert.myapplication2.R;
import com.example.evert.myapplication2.data.EsteveDataSource;
//import com.bossturban.webviewmarker.TextSelectionSupport;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.XMLReader;

import java.lang.reflect.Method;


public class ContenidoFragment extends Fragment  {
    TextView contenedor;
    WebView webView;
    //private TextSelectionSupport mTextSelectionSupport;
    static final String FRAGMENT_ACTUAL = "fragment_actual";
    public static final String TAG_NAME = "CONTENIDO_FRAGMENT";
    private View root;
    private String titulo = "prueba";
    private String seleccion = "";
    private ActionMode action;
    private EsteveDataSource dataSource;
    private Integer contenido_id;
    private ActionMode mActionMode = null;
    private String contenido_articulo = "";


    public ContenidoFragment() {
        // Required empty public constructor
    }

    public static ContenidoFragment newInstance(Bundle args){
        ContenidoFragment fragment = new ContenidoFragment();
        if (args != null){
            fragment.setArguments(args);
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        root =  inflater.inflate(R.layout.fragment_contenido, container, false);
        setHasOptionsMenu(true);
        contenedor = (TextView) root.findViewById(R.id.informacion);
        contenedor.setText(Html.fromHtml("<br><p>Description <b>here<b></p>"));
        dataSource = new EsteveDataSource(this.root.getContext());


        final Bundle bundle=getArguments();
        if(bundle!=null){
            if (bundle.containsKey("contenido")){
                contenido_id = bundle.getInt("IdContenido");
                contenido_articulo = dataSource.getContenidoArticulo(contenido_id);
                String array_targets = null;
                JSONObject jsonObject = dataSource.getTargetSelection(contenido_id);
                JSONArray comentarios;
                String comentario_text = "";
                try {
                    array_targets = jsonObject.getString("tagerts");
                    comentarios = jsonObject.getJSONArray("comentarios");
                    for(int i = 0; i < comentarios.length();i++){
                        comentario_text+="<p id='"+comentarios.getJSONObject(i).getInt("id")+"' style='display:none'>"+ comentarios.getJSONObject(i).getString("comentario")+"</p>";
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }







                String html_text = "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />" +
                            "<link rel=\"stylesheet\" href=\"css/main.css\"> " +
                            "<link rel=\"stylesheet\" href=\"css/foundation.min.css\"> " +
                            "<style type='text/css'>.highlight {background-color: yellow;} .note{background-color: green;} #summary {border: dotted orange 1px;}</style>"+
                            "<script src='js/jquery-1.8.3.js'></script>"+
                            "<script src='js/jpntext.js'></script>"+
                            "<script src='js/rangy-core.js'></script>"+
                            "<script src='js/rangy-serializer.js'></script>"+
                            "<script src='js/android.selection.js'></script>"+
                            "<script src='js/rangy-classapplier.js'></script>"+
                            "<script src='js/rangy-highlighter.js'></script>"+



                            "</head>" +
                            "<body style='padding-bottom: 120px;'>" +
                                "<div class=\"guia-wrap \" id='contenedor'>"+
                                    contenido_articulo+

                                "<div>" +
                            "<div id='notas-group'>"+
                            comentario_text+
                            "</div>"+
                            "<div class='small reveal' id='exampleModal' data-reveal>" +
                                "<p id='cuerpo-modal' ></p><button class='close-button' data-close aria-label='Close modal' type='button'><span aria-hidden='true'>&times;</span></button>"+
                            "</div>"+
                            "<p id='targetSelection' style='display:none'>type:textContent|"+array_targets+"</p>"+
                            "<script src='js/foundation.min.js'></script>"+
                            "<script src='js/utilidades.js'></script>"+
                            //"<script src='js/jquery.js'></script>"+
                            //"<script src='js/rangy-core.js'></script>"+
                            //"<script src='js/rangy-serializer.js'></script>"+
                            //"<script src='js/android.selection.js'></script>"+
                            "</body>" +


                            "</html>";

                webView = (WebView) root.findViewById(R.id.webView);
                webView.setLongClickable(false);
                Log.e("ContenidoFragment", "Targats: " + array_targets);
                webView.setWebChromeClient(new WebChromeClient());
                webView.getSettings().setJavaScriptEnabled(true);
                webView.getSettings().setAllowContentAccess(true);
                webView.getSettings().setAllowFileAccess(true);
                webView.loadDataWithBaseURL("file:///android_asset/", html_text, "text/html", "utf-8", "");

                webView.setWebViewClient(new WebViewClient(){
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        if(isURLMatching(url)){

                            return true;
                        }

                        return super.shouldOverrideUrlLoading(view,url);
                    }

                });
                //setHasOptionsMenu(true);


            }else{
                String html_text = "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />" +
                        "<link rel=\"stylesheet\" href=\"css/main.css\"> " +
                        "<link rel=\"stylesheet\" href=\"css/foundation.min.css\"> " +
                        "<style type='text/css'>.highlight {background-color: yellow;} .note{background-color: green;} #summary {border: dotted orange 1px;}</style>"+
                        "<script src='js/jquery-1.8.3.js'></script>"+
                        "<script src='js/jpntext.js'></script>"+
                        "<script src='js/rangy-core.js'></script>"+
                        "<script src='js/rangy-serializer.js'></script>"+
                        "<script src='js/android.selection.js'></script>"+
                        "<script src='js/rangy-classapplier.js'></script>"+
                        "<script src='js/rangy-highlighter.js'></script>"+



                        "</head>" +
                        "<body style='padding-bottom: 120px;'>" +
                        "<div class=\"guia-wrap \" id='contenedor'><p>Hola mundo<strong> con</strong> selecciones</p>"+


                        "<div>" +

                        "<div class='small reveal' id='exampleModal' data-reveal>" +
                        "<p id='cuerpo-modal' ></p><button class='close-button' data-close aria-label='Close modal' type='button'><span aria-hidden='true'>&times;</span></button>"+
                        "</div>"+
                        "<p id='targetSelection' style='display:none'>type:textContent</p>"+
                        "<script src='js/foundation.min.js'></script>"+
                        "<script src='js/utilidades.js'></script>"+
                        //"<script src='js/jquery.js'></script>"+
                        //"<script src='js/rangy-core.js'></script>"+
                        //"<script src='js/rangy-serializer.js'></script>"+
                        //"<script src='js/android.selection.js'></script>"+
                        "</body>" +


                        "</html>";

                webView = (WebView) root.findViewById(R.id.webView);
                webView.setLongClickable(false);
                webView.setWebChromeClient(new WebChromeClient());
                webView.getSettings().setJavaScriptEnabled(true);
                webView.getSettings().setAllowContentAccess(true);
                webView.getSettings().setAllowFileAccess(true);
                webView.loadDataWithBaseURL("file:///android_asset/", html_text, "text/html", "utf-8", "");

                webView.setWebViewClient(new WebViewClient(){
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        if(isURLMatching(url)){

                            return true;
                        }

                        return super.shouldOverrideUrlLoading(view,url);
                    }

                });
            }

            if (bundle.containsKey("titulo")){
                getActivity().getActionBar().setTitle(bundle.getString("titulo"));
            }


        }else{
            String html_text = "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />" +
                    "<link rel=\"stylesheet\" href=\"css/main.css\"> " +
                    "<link rel=\"stylesheet\" href=\"css/foundation.min.css\"> " +
                    "<style type='text/css'>.highlight {background-color: yellow;} .note{background-color: green;} #summary {border: dotted orange 1px;}</style>"+
                    "<script src='js/jquery-1.8.3.js'></script>"+
                    "<script src='js/jpntext.js'></script>"+
                    "<script src='js/rangy-core.js'></script>"+
                    "<script src='js/rangy-serializer.js'></script>"+
                    "<script src='js/android.selection.js'></script>"+
                    "<script src='js/rangy-classapplier.js'></script>"+
                    "<script src='js/rangy-highlighter.js'></script>"+



                    "</head>" +
                    "<body style='padding-bottom: 120px;'>" +
                    "<div class=\"guia-wrap \" id='contenedor'><p>Hola mundo<strong> con</strong> selecciones</p>"+


                    "<div>" +

                    "<div class='small reveal' id='exampleModal' data-reveal>" +
                    "<p id='cuerpo-modal' ></p><button class='close-button' data-close aria-label='Close modal' type='button'><span aria-hidden='true'>&times;</span></button>"+
                    "</div>"+
                    "<p id='targetSelection' style='display:none'>type:textContent</p>"+
                    "<script src='js/foundation.min.js'></script>"+
                    "<script src='js/utilidades.js'></script>"+
                    //"<script src='js/jquery.js'></script>"+
                    //"<script src='js/rangy-core.js'></script>"+
                    //"<script src='js/rangy-serializer.js'></script>"+
                    //"<script src='js/android.selection.js'></script>"+
                    "</body>" +


                    "</html>";

            webView = (WebView) root.findViewById(R.id.webView);
            webView.setLongClickable(false);
            webView.setWebChromeClient(new WebChromeClient());
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setAllowContentAccess(true);
            webView.getSettings().setAllowFileAccess(true);
            webView.loadDataWithBaseURL("file:///android_asset/", html_text, "text/html", "utf-8", "");

            webView.setWebViewClient(new WebViewClient(){
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    if(isURLMatching(url)){

                        return true;
                    }

                    return super.shouldOverrideUrlLoading(view,url);
                }

            });
        }




        return root;
    }



    public void setTitulo(String titulo){
        this.titulo =  titulo;
    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the user's current game state
        savedInstanceState.putInt(FRAGMENT_ACTUAL,4);
        Log.e("Almacenado", String.valueOf(4));

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }


    private void openAlert(View view,final ActionMode mode) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this.getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View root = inflater.inflate(R.layout.dialogo_layout, null);
        alertDialogBuilder.setTitle("Nota:");
        alertDialogBuilder.setView(root);
        final TextView nota_contenido = (TextView) root.findViewById(R.id.nota_cuerpo);
        // set positive button: Yes message
        alertDialogBuilder.setPositiveButton("Guardar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                String script_nota = "function nota_select(){ var sal =  noteSelectedText('"+nota_contenido.getText()+"'); return sal;}; nota_select();";
                webView.evaluateJavascript(script_nota, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {

                        String selectedData = s; //value of javascript return

                        //String[] array = selectedData.split("[$]");


                        dataSource.insertSelection("1", contenido_id, "2", selectedData.replace("\"", ""), nota_contenido.getText().toString());
                        Log.e("WebView runtime", selectedData);
                        mode.finish();
                    }
                });
                Log.e("ContenidoFragment", nota_contenido.getText().toString());

            }
        });
        // set negative button: No message
        alertDialogBuilder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // cancel the alert box and put a Toast to the user
                dialog.cancel();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show alert
        alertDialog.show();
    }

    public void resaltarContenido(){
        String script = "function select(){ var sal =  highlightSelectedText(); return sal;}; select();";
        webView.evaluateJavascript(script, new ValueCallback<String>() {
            @Override
            public void onReceiveValue(String s) {
                String selectedData = s; //value of javascript return
                String[] array = selectedData.split("[$]");
                dataSource.insertSelection("1", contenido_id, "1", selectedData.replace("\"", ""), "");
                Log.e("WebView runtime", selectedData);
            }
        });
    }

    public void realizarNota(ActionMode actionMode){
        openAlert(root, actionMode);
    }

    public void eliminarSeleccion(){
        String script = "function eliminar_seleccion(){ var salida='';var sal =  removeHighlightFromSelectedText(); $.each(sal,function(index,value){if(salida_id!=''){salida_id +='$'}salida_id += value.id}); return sal;}; eliminar_seleccion();";
        webView.evaluateJavascript(script, new ValueCallback<String>() {
            @Override
            public void onReceiveValue(String s) {
                String selectedData = s; //value of javascript return
                try {
                    JSONArray jsonArray = new JSONArray(s);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject explrObject = jsonArray.getJSONObject(i);
                        dataSource.deleteSelection(explrObject.getInt("id"), contenido_id);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.e("WebView runtime", selectedData);
            }


        });
    }

    protected boolean isURLMatching(String url) {
        // some logic to match the URL would be safe to have here
        String[] array = url.split("[#]");
        Log.e("isURLMatching",array[array.length-1]);
        return true;
    }

    protected void openNextActivity() {
        /*Intent intent = new Intent(this, MyNextActivity.class);
        startActivity(intent);*/
        Log.e("ContenidoFragment","Se debe abrir el alert con la ifnormacion de la nota");
    }

}
