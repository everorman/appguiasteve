package com.example.evert.myapplication2.activities;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.example.evert.myapplication2.R;
import com.example.evert.myapplication2.fragments.FormIndexFragment;
import com.example.evert.myapplication2.fragments.IndexFragment;
import com.viewpagerindicator.CirclePageIndicator;


public class MainActivity extends FragmentActivity {

    static int numberOfPages = 2;
    ViewPager myViewPager;

    MyFragmentPagerAdapter myFragmentPagerAdapter;
    String text = "test";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myViewPager = (ViewPager) findViewById(R.id.pager);
        myFragmentPagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager());
        myViewPager.setAdapter(myFragmentPagerAdapter);
        //CirclePageIndicator mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
        //mIndicator.setViewPager(myViewPager);
    }


    public void onClickEvent(View view){
        Intent inten = new Intent(this,BaseActivity.class);
        startActivity(inten);
    }


    private static class MyFragmentPagerAdapter extends FragmentPagerAdapter {
        private Fragment[] fragments = new Fragment[]{
                new IndexFragment(),
                new FormIndexFragment()
        };

        public MyFragmentPagerAdapter(FragmentManager fm)   {
            super(fm);
        }

        @Override
        public android.support.v4.app.Fragment getItem(int index) {
            return fragments[index];
        }

        @Override
        public int getCount() {
            return numberOfPages;
        }
    }
}
