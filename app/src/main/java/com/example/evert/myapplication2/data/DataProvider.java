package com.example.evert.myapplication2.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Evert on 24-09-2015.
 */
public class DataProvider {

    public static HashMap<String, List<String>> getItem1()
    {
        HashMap<String, List<String>> Details = new HashMap<String, List<String>>();

        List<String> items_1_1 = new ArrayList<String>();
        items_1_1.add("Concepto de interacción farmacológica");
        items_1_1.add("Frecuencia y métodos de estudio de las interacciones farmacológicas");
        items_1_1.add("Diagnóstico de las interacciones farmacológicas");
        items_1_1.add("Mecanismos de producción de las interacciones");
        items_1_1.add("Interacciones por modificación del metabolismo");

        List<String> items_1_2 = new ArrayList<String>();
        items_1_2.add("Participación del citocromo P450");
        items_1_2.add("Papel de los transportadores");


        List<String> items_1_3 = new ArrayList<String>();
        items_1_3.add("Lorem ipsum dolor sit amet, consectetuer");
        items_1_3.add("Lorem ipsum dolor sit amet, consectetuer");
        items_1_3.add("Lorem ipsum dolor sit amet, consectetuer");
        items_1_3.add("Lorem ipsum dolor sit amet, consectetuer");
        items_1_3.add("Lorem ipsum dolor sit amet, consectetuer");

        List<String> items_1_4 = new ArrayList<String>();
        items_1_4.add("Lorem ipsum dolor sit amet, consectetuer");
        items_1_4.add("Lorem ipsum dolor sit amet, consectetuer");
        items_1_4.add("Lorem ipsum dolor sit amet, consectetuer");


        Details.put("Introducción", items_1_1);
        Details.put("Interacciones farmacocinéticas de las estainas", items_1_2);
        Details.put("Interacciones de la práctica clínica",items_1_3);
        Details.put("Bibliografia",items_1_4);

        return Details;

    }

    public static HashMap<String, List<String>> getItem2()
    {
        HashMap<String, List<String>> Details = new HashMap<String, List<String>>();

        List<String> items_1_1 = new ArrayList<String>();
        items_1_1.add("Concepto de interacción farmacológica");
        items_1_1.add("Frecuencia y métodos de estudio de las interacciones farmacológicas");
        items_1_1.add("Diagnóstico de las interacciones farmacológicas");
        items_1_1.add("Mecanismos de producción de las interacciones");
        items_1_1.add("Interacciones por modificación del metabolismo");

        List<String> items_1_2 = new ArrayList<String>();
        items_1_2.add("Participación del citocromo P450");
        items_1_2.add("Papel de los transportadores");


        List<String> items_1_3 = new ArrayList<String>();
        items_1_3.add("Lorem ipsum dolor sit amet, consectetuer");
        items_1_3.add("Lorem ipsum dolor sit amet, consectetuer");
        items_1_3.add("Lorem ipsum dolor sit amet, consectetuer");
        items_1_3.add("Lorem ipsum dolor sit amet, consectetuer");
        items_1_3.add("Lorem ipsum dolor sit amet, consectetuer");

        List<String> items_1_4 = new ArrayList<String>();
        items_1_4.add("Lorem ipsum dolor sit amet, consectetuer");
        items_1_4.add("Lorem ipsum dolor sit amet, consectetuer");
        items_1_4.add("Lorem ipsum dolor sit amet, consectetuer");


        Details.put("Introducción", items_1_1);
        Details.put("Interacciones farmacocinéticas de las estainas", items_1_2);
        Details.put("Interacciones de la práctica clínica",items_1_3);
        Details.put("Bibliografia",items_1_4);

        return Details;

    }

    public static HashMap<String, List<String>> getItem3()
    {
        HashMap<String, List<String>> Details = new HashMap<String, List<String>>();

        List<String> items_1_1 = new ArrayList<String>();
        items_1_1.add("Concepto de interacción farmacológica");
        items_1_1.add("Frecuencia y métodos de estudio de las interacciones farmacológicas");
        items_1_1.add("Diagnóstico de las interacciones farmacológicas");
        items_1_1.add("Mecanismos de producción de las interacciones");
        items_1_1.add("Interacciones por modificación del metabolismo");

        List<String> items_1_2 = new ArrayList<String>();
        items_1_2.add("Participación del citocromo P450");
        items_1_2.add("Papel de los transportadores");


        List<String> items_1_3 = new ArrayList<String>();
        items_1_3.add("Lorem ipsum dolor sit amet, consectetuer");
        items_1_3.add("Lorem ipsum dolor sit amet, consectetuer");
        items_1_3.add("Lorem ipsum dolor sit amet, consectetuer");
        items_1_3.add("Lorem ipsum dolor sit amet, consectetuer");
        items_1_3.add("Lorem ipsum dolor sit amet, consectetuer");

        List<String> items_1_4 = new ArrayList<String>();
        items_1_4.add("Lorem ipsum dolor sit amet, consectetuer");
        items_1_4.add("Lorem ipsum dolor sit amet, consectetuer");
        items_1_4.add("Lorem ipsum dolor sit amet, consectetuer");


        Details.put("Introducción", items_1_1);
        Details.put("Interacciones farmacocinéticas de las estainas", items_1_2);
        Details.put("Interacciones de la práctica clínica",items_1_3);
        Details.put("Bibliografia",items_1_4);

        return Details;

    }


}
