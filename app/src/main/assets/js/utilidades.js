var serializedHighlights = document.getElementById('targetSelection').innerHTML;
        var highlighter;

        var initialDoc;

        window.onload = function() {
            rangy.init();

            highlighter = rangy.createHighlighter();

            highlighter.addClassApplier(rangy.createClassApplier("highlight", {
                ignoreWhiteSpace: true,
                tagNames: ["span", "a"]
            }));

            highlighter.addClassApplier(rangy.createClassApplier("note", {
                ignoreWhiteSpace: true,

                elementTagName: "span",
                elementProperties: {
                        href: "#nota",
                        onclick: function() {
                            var highlight = highlighter.getHighlightForElement(this);
                            $(document).foundation();
                            $("#cuerpo-modal").text($('#'+highlight.id).text());
                            var popup = new Foundation.Reveal($('#exampleModal'));
                            popup.open();
                            /*if (window.confirm($('#'+highlight.id).text())) {
                                highlighter.removeHighlights( [highlight] );
                            }*/
                            return false;
                        }
                    }

            }));


            if (serializedHighlights) {
                highlighter.deserialize(serializedHighlights);
                return serializedHighlights;
            }
        };


        function highlightSelectedText() {
            var s = "";
            var options = new Object();
            options.containerElementId = "contenedor";
            var salida = highlighter.highlightSelection("highlight",options);
            s = salida[0]['characterRange']['start']+'$'+salida[0]['characterRange']['end']+'$'+salida[0]['id']+'$highlight$'
            return s;
        }

        function noteSelectedText(nota) {
            var s = "initial value";
            var options = new Object();
            options.containerElementId = "contenedor";
            var salida = highlighter.highlightSelection("note",options);
            s = salida[0]['characterRange']['start']+'$'+salida[0]['characterRange']['end']+'$'+salida[0]['id']+'$note$'
            $('#notas-group').append('<p id='+salida[0]['id']+'>'+nota+'</p>');
            return s;
        }

        function removeHighlightFromSelectedText() {
            return highlighter.unhighlightSelection();
        }
        var salida_id="";
        function highlightScopedSelectedText() {

           var resultado = highlighter.highlightSelection("highlight", { containerElementId: "summary" });

           return resultado;
        }

        function noteScopedSelectedText() {
            highlighter.highlightSelection("note", { containerElementId: "summary" });

        }

        function reloadPage(button) {
            button.form.elements["serializedHighlights"].value = highlighter.serialize();
            button.form.submit();
        }

        function loadSelection(){

                 //var salida = 'type:textContent|'+targets;
                 //highlighter.deserialize(salida);
                 //console.log("Salida");
                 return "Ejecutando funcion";


        }

        function click_selector(){
            clickedBtnID = "salida";
            $(document).on("click", function () {
               clickedBtnID = $(this).html(); // or var clickedBtnID = this.id
               alert('you clicked on button #' + clickedBtnID);
            });
            return clickedBtnID;

        }
